import 'dart:collection';


class Transaction {
  final int id;
  final String category;
  final String description;
  final String amount;
  final String currency;
  final String transactionTime;

  Transaction({this.category, this.id, this.description, this.amount, this.currency, this.transactionTime});

  factory Transaction.fromJson(Map<String, dynamic> json) {
    return Transaction(
      id: json['id'],
      category: json['category'],
      description: json['description'],
      amount: json['amount'],
      currency: json['currency'],
      transactionTime: json['transactionTime'],
    );
  }
}

class TransactionsModel {
  List<Transaction> rawTransactions;
  HashMap<String, double> sortedData = new HashMap();

  TransactionsModel(this.rawTransactions) {
    for (Transaction transaction in rawTransactions) {
      var amount = double.parse(transaction.amount);
      if (sortedData.containsKey(transaction.category)) {
        sortedData[transaction.category] += amount;
      } else {
        sortedData[transaction.category] = amount;
      }
    }
  }

  Transaction getGenericTransaction(int position) {
    int i = 0;
    for(String key in sortedData.keys) {
      if(i == position) {
        return new Transaction(category : key, amount: sortedData[key].toString());
      }
      i++;
    }
    return new Transaction();
  }
}

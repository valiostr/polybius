import 'package:flutter_app_polybius/network/transactionResponse.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';


Future<TransactionsModel> fetchTransactions() async {
  final response =
      await http.get('http://www.mocky.io/v2/5d18ee46300000a5458beba8');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON.
    List responseJson = json.decode(response.body);
    return new TransactionsModel(responseJson.map((m) => new Transaction.fromJson(m)).toList());
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load transactions');
  }
}

import 'dart:collection';

import "package:flutter/material.dart";
import 'dart:math';

const BLUE_NORMAL = Color(0xff54c5f8);
const GREEN_NORMAL = Color(0xff6bde54);
const BLUE_DARK2 = Color(0xff01579b);
const BLUE_DARK1 = Color(0xff29b6f6);
const RED_DARK1 = Color(0xfff26388);
const RED_DARK2 = Color(0xfff782a0);
const RED_DARK3 = Color(0xfffb8ba8);
const RED_DARK4 = Color(0xfffb89a6);
const RED_DARK5 = Color(0xfffd86a5);
const YELLOW_NORMAL = Color(0xfffcce89);
const List<Point> POINT = [Point(100, 100)];

class CirclePainter extends CustomPainter {
  CirclePainter(
      {this.startAngle, this.radiants, this.colorsOfChart, this.tapPosition});

  final double startAngle;
  final List<double> radiants;
  final int tapPosition;

  List<Color> colorsOfChart;

  int circleSize = 500;
  int circleCenterOffset = 250;
  int circleRadius = 200;
  int painterSize = 300;

  @override
  void paint(Canvas canvas, Size size) {
    double circleConvertedOffset =
        (circleCenterOffset * size.width) / circleSize;
    double circleConvertedRadius = circleRadius *
        sqrt((size.width * size.width + size.width * size.width) /
            (circleSize * circleSize + circleSize * circleSize));

    var paint = Paint()
      ..style = PaintingStyle.fill
      ..color = BLUE_NORMAL
      ..strokeWidth = 2.0
      ..isAntiAlias = true;
    paint.color = YELLOW_NORMAL;

    paint.color = RED_DARK1;
    paint.strokeWidth = 20;
    paint.style = PaintingStyle.stroke;
    var center = Offset(
      circleConvertedOffset,
      circleConvertedOffset,
    );
    drawArcGroup(
      canvas,
      center: center,
      radius: circleConvertedRadius,
      paintWidth: 80.0,
      startAngle: 1.3 * startAngle / circleConvertedRadius,
      hasEnd: false,
      hasCurrent: false,
      curPaintWidth: 95.0,
    );

    canvas.save();
    canvas.restore();
  }

  void drawArcGroup(Canvas canvas,
      {Offset center,
      double radius,
      double startAngle = 0.0,
      double paintWidth = 10.0,
      bool hasEnd = false,
      hasCurrent = false,
      curPaintWidth = 12.0}) {
    assert(colorsOfChart != null && colorsOfChart.length > 0);
    var paint = Paint()
      ..style = PaintingStyle.fill
      ..color = BLUE_NORMAL
      ..strokeWidth = paintWidth
      ..isAntiAlias = true;

    var startA = startAngle;
    paint.style = PaintingStyle.stroke;
    var curStartAngle = 0.0;

    for (int i = 0; i < radiants.length; i++) {
      var rd = radiants[i];

      if (i == tapPosition) {
        curStartAngle = startA;
        hasCurrent = true;
      } else {
        paint.color = colorsOfChart[i % colorsOfChart.length];

        paint.strokeWidth = paintWidth;
        drawArcWithCenter(canvas, paint,
            center: center,
            radius: radius,
            startRadian: startA,
            sweepRadian: rd);
      }

      startA += rd;
    }

    if (hasCurrent) {
      paint.color = colorsOfChart[tapPosition % colorsOfChart.length];
      paint.strokeWidth = curPaintWidth;
      paint.style = PaintingStyle.stroke;
      drawArcWithCenter(canvas, paint,
          center: center,
          radius: radius,
          startRadian: curStartAngle,
          sweepRadian: radiants[tapPosition]);
    }
  }

  void drawArcWithCenter(
    Canvas canvas,
    Paint paint, {
    Offset center,
    double radius,
    startRadian = 0.0,
    sweepRadian = pi,
  }) {
    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radius),
      startRadian,
      sweepRadian,
      false,
      paint,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

import 'dart:collection';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_app_polybius/network/transactionResponse.dart';
import 'package:flutter_app_polybius/network/transactionsRequest.dart';
import 'package:flutter_app_polybius/screens/detailsScreen.dart';
import 'package:flutter_app_polybius/views/painter.dart';
import 'arguments/detailsScreenArguments.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Canvas',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  CircleState createState() => CircleState();
}

enum TapElement { OUT, CATEGORY, SELECTED }

class CircleState extends State<HomePage> {
  String categoryDescription = "";
  String categoryName = "";
  double startAngle = 0.0;
  int tapPosition;
  Offset tapOffset = Offset(0, 0);
  GlobalKey paintKey = new GlobalKey();
  Future<TransactionsModel> transactionRequest = fetchTransactions();
  Color categoryColor = Colors.white;
  TransactionsModel transactionModel;
  List<Color> colors = [
    BLUE_DARK1,
    RED_DARK1,
    BLUE_DARK2,
    GREEN_NORMAL,
    YELLOW_NORMAL
  ];

  List<double> getDataForPainter() {
    HashMap<String, double> source = transactionModel.sortedData;
    double total = 0;
    for (String category in source.keys) {
      total += source[category];
    }

    assert(total > 0.0);
    List<double> radians = List<double>();

    for (String category in source.keys) {
      double radian = source[category] * 2 * pi / total;
      radians.add(radian);
    }
    return radians;
  }

  int getPositionOfTap(List<double> radians) {
    double x = tapOffset.dx - 150;
    double y = tapOffset.dy - 150;
    double radiantOfPoint = atan2(y, x);
    if (radiantOfPoint < -0.1) {
      radiantOfPoint = radiantOfPoint + 6.24828;
    }

    double startAngle = 0.0;
    for (int i = 0; i < radians.length; i++) {
      var rd = radians[i];
      if (startAngle < radiantOfPoint && radiantOfPoint < (startAngle + rd)) {
        return i;
      }
      startAngle += rd;
    }

    return -1;
  }

  TapElement getTapedPlace() {
    if (tapOffset != null) {
      if ((tapOffset.dx - 150) * (tapOffset.dx - 150) +
              (tapOffset.dy - 150) * (tapOffset.dy - 150) <=
          80 * 80) {
        return TapElement.SELECTED;
      }

      if ((tapOffset.dx - 150) * (tapOffset.dx - 150) +
              (tapOffset.dy - 150) * (tapOffset.dy - 150) <=
          160 * 160) {
        return TapElement.CATEGORY;
      } else {
        return TapElement.OUT;
      }
    }
    return TapElement.OUT;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("General expances"),
      ),
      body: Container(
          child: Center(
              child: Listener(
        onPointerDown: (PointerDownEvent event) {
          RenderBox referenceBox = paintKey.currentContext.findRenderObject();
          Offset offset = referenceBox.globalToLocal(event.position);

          tapOffset = offset;

          switch (getTapedPlace()) {
            case TapElement.CATEGORY:
              tapPosition = getPositionOfTap(getDataForPainter());
              if (tapPosition > -1) {
                categoryColor = colors[tapPosition % colors.length];
                Transaction genericTransaction =
                    transactionModel.getGenericTransaction(tapPosition);
                categoryName = genericTransaction.category;
                var categoryAmount = genericTransaction.amount;
                categoryDescription = "$categoryName \n $categoryAmount";
              }
              break;
            case TapElement.OUT:
              break;
            case TapElement.SELECTED:
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DetailPage(),
                  settings: RouteSettings(
                    arguments: DetailsScreenArgument(
                        categoryName,
                        transactionModel.rawTransactions
                            .where((t) => t.category == categoryName)
                            .toList()),
                  ),
                ),
              );
              break;
          }
          setState(() {});
        },
        child: Container(
          width: 300,
          height: 300,
          key: paintKey,
          child: FutureBuilder<TransactionsModel>(
            future: transactionRequest,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                transactionModel = snapshot.data;
                return Stack(
                  children: <Widget>[
                    CustomPaint(
                      painter: CirclePainter(
                          startAngle: startAngle,
                          colorsOfChart: colors,
                          tapPosition: tapPosition,
                          radiants: getDataForPainter()),
                      child: Container(),
                    ),
                    Center(
                      child: Container(
                        width: 150,
                        height: 150,
                        decoration: new BoxDecoration(
                          color: categoryColor,
                          boxShadow: [
                            new BoxShadow(
                                color: Colors.red,
                                blurRadius: 5.0,
                                spreadRadius: 5.0)
                          ],
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                    Center(
                      child: Text(
                        categoryDescription,
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                );
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }

              // By default, show a loading spinner.
              return CircularProgressIndicator();
            },
          ),
        ),
      ))),
    );
  }
}

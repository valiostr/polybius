import 'package:flutter/material.dart';
import 'package:flutter_app_polybius/network/transactionResponse.dart';
import 'package:intl/intl.dart';
import 'arguments/detailsScreenArguments.dart';


class DetailPage extends StatefulWidget {
  @override
  DetailState createState() => DetailState();
}

class DetailState extends State<DetailPage> {
  List<Transaction> data;
  var formatter = DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

  sortByAmount() {
    data.sort((a, b) => a.amount.compareTo(b.amount));
    setState(() {});
  }

  sortByDescription() {
    data.sort((a, b) => a.description.compareTo(b.description));
    setState(() {});
  }

  sortByDate() {
    data.sort((a, b) => formatter
        .parse(a.transactionTime, true)
        .compareTo(formatter.parse(b.transactionTime, true)));
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final DetailsScreenArgument args =
        ModalRoute.of(context).settings.arguments;
    data = args.transactionsList;
    return Scaffold(
        appBar: AppBar(
          title: Text(args.title),
        ),
        body: new Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Text("by amount"),
                  onPressed: sortByAmount,
                ),
                RaisedButton(
                  child: Text("by description"),
                  onPressed: sortByDescription,
                ),
                RaisedButton(
                  child: Text("by date"),
                  onPressed: sortByDate,
                ),
              ],
            ),
            new Expanded(
              child: GridView.count(
                crossAxisCount: 2,
                children: List.generate(data.length, (index) {
                  return Center(
                      child: Container(
                    width: 150,
                    height: 150,
                    decoration: BoxDecoration(
                      color: Colors.black12,
                      shape: BoxShape.rectangle,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            blurRadius: 2.0,
                            spreadRadius: 1.0)
                      ],
                    ),
                    child: new Column(
                      children: [
                        Text(
                          '${data[index].description}',
                          style: Theme.of(context).textTheme.headline,
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          '${data[index].amount}',
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          data[index].currency,
                          textAlign: TextAlign.center,
                        ),
                        Text(
                          new DateFormat("yyyy-MM-dd hh:mm").format(
                              formatter.parse(data[index].transactionTime)),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ));
                }),
              ),
            ),
          ],
        ));
  }
}

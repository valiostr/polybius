
import 'package:flutter_app_polybius/network/transactionResponse.dart';


class DetailsScreenArgument{
    final String title;
    final List<Transaction> transactionsList;

    DetailsScreenArgument(this.title, this.transactionsList);
}